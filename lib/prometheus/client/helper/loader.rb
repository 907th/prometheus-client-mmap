module Prometheus
  module Client
    module Helper
      module Loader
        class << self
          def rust_impl_available?
            return @rust_available unless @rust_available.nil?

            check_for_rust
          end

          private

          def load_rust_extension
            begin
              ruby_version = /(\d+\.\d+)/.match(RUBY_VERSION)
              require_relative "../../../#{ruby_version}/fast_mmaped_file_rs"
            rescue LoadError
              require 'fast_mmaped_file_rs'
            end
          end

          def check_for_rust
            # This will be evaluated on each invocation even with `||=` if
            # `@rust_available` if false. Running a `require` statement is slow,
            # so the `rust_impl_available?` method memoizes the result, external
            # callers can only trigger this method a single time.
            @rust_available = begin
              load_rust_extension
              true
            rescue LoadError
              warn <<~WARN
WARNING: The Rust extension for prometheus-client-mmap is unavailable, falling back to the legacy C extension.
  The Rust extension will be required in the next version. If you are compiling this gem from source,
  ensure your build system has a Rust compiler and clang: https://gitlab.com/gitlab-org/ruby/gems/prometheus-client-mmap
WARN
              false
            end
          end
        end
      end
    end
  end
end
